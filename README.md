# Jekyll-103

Test playground starting from local according to [this tutorial](https://gitlab.com/pages/jekyll)

---
1. create repo in GitLab
2. git clone SSH
3. open local folder
4. save workspace
5. commit and sync
6. create Jekyll project:
```
bundle init
bundle add jekyll
bundle exec jekyll new --force --skip-bundle .
bundle install
bundle exec jekyll serve
```

project works in localhost under template minima

7. commit everything and push
8. add .gitlab-ci.yml

il problema è che mentre in localhost tutto è ok il sito prodotto da GitLab 
Pages perde il template ed i link

Su Render tutto ok. Adesso faccio due test:
- tolgo .gitlab-ci.yml
- tolgo la pagina (sempre dopo il precedente sia ancora lì)

### new theme
[tutorial](https://www.chrisanthropic.com/blog/2016/creating-gem-based-themes-for-jekyll/)
- updated command `bundle exec jekyll new-theme myTheme1`